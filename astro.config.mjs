import { defineConfig } from 'astro/config'

// https://astro.build/config
export default defineConfig({
  site: 'https://paxa.dev',
  outDir: 'public',
  publicDir: 'static'
})